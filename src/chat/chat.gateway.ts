import { Logger } from "@nestjs/common";
import { OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit, SubscribeMessage, WebSocketGateway, WebSocketServer } from "@nestjs/websockets";
import { Server, Socket } from "socket.io";
import { v4 } from "uuid";

@WebSocketGateway()
export class ChatGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    private logger: Logger = new Logger("ChatGateway");
    private newRooms: string[] = [];
    @WebSocketServer() server: Server;

    afterInit(server: Server) {
        this.logger.log('Chat Gateway Initialized!');
    }

    handleConnection(client: Socket, ...args: any[]) {
        this.logger.log(`Client connected: ${client.id}`);
    }

    handleDisconnect(client: Socket) {
        this.logger.log(`Client disconnected: ${client.id}`);
    }

    @SubscribeMessage('joinRoom')
    joinRoom(client: Socket) {
        let roomId: string = '';
        if(this.newRooms.length > 0) {
            roomId = this.newRooms.shift();
        } else {
            roomId = v4();
            this.newRooms.push(roomId);
        }
        client.join(roomId);
        client.emit('joinRoom', roomId);
    }

    @SubscribeMessage('newMessage')
    handleMessage(client: Socket, data: { room: string, text: string }): void {
        this.server.to(data.room).emit(data.room, data.text);
    }
}